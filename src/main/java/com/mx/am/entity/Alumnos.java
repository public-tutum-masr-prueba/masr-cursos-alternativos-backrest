package com.mx.am.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@Table(name = "t_alumnos", catalog = "escuela")
public class Alumnos implements Serializable {
	
	private static final long serialVersionUID = 5264367418885392363L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_t_usuarios")
	private Long idUsuario;
	
	@Column(name = "nombre", length = 80)
	private String nombre;
	
	@Column(name = "ap_paterno", length = 80)
	private String apPaterno;
	
	@Column(name = "ap_materno", length = 80)
	private String apMaterno;
	
	@Column(name = "activo", length = 80)
	private boolean activo;
	
	@Override
	public String toString() {
		return "Alumnos [idUsuario=" + idUsuario + ", nombre=" + nombre + ", apPaterno=" + apPaterno + ", apMaterno="
				+ apMaterno + ", activo=" + activo + "]";
	}
	
}
