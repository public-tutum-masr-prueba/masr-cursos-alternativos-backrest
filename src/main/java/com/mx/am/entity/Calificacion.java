package com.mx.am.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "t_calificaciones", catalog = "escuela")
public class Calificacion implements Serializable {
	
	private static final long serialVersionUID = -5389331535161707866L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_t_calificaciones")
	private Long idTCalificaciones;
	
	//@ManyToMany(fetch = FetchType.LAZY)
	//@OneToOne(fetch = FetchType.LAZY) //id_t_materias @OneToOne(mappedBy="idTMaterias")
	//@JoinTable(name = "calificacions_to_materias", joinColumns = @JoinColumn(name="id_t_materias"))
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_t_materias", referencedColumnName = "id_t_materias" ) //referencedColumnName=Nombre de la columna de la tabla MAteria
	private Materia materia;
	
	@ManyToOne //id_t_usuarios @OneToOne(mappedBy="idTUsuario")
	@JoinColumn(name = "id_t_usuarios", referencedColumnName = "id_t_usuarios" )
	private Alumnos alumnos;
	
	private Double calificacion;
	
	@Column(name = "fecha_registro")
	private Date fechaRegistro;
	
	//@Transient
	//private Double promedio;
	
	@Override
	public String toString() {
		return "Calificacion [idTCalificaciones=" + idTCalificaciones + ", materia=" + materia + ", alumnos=" + alumnos
				+ ", calificacion=" + calificacion + ", fechaRegistro=" + fechaRegistro + /*", promedio=" + promedio +*/ "]";
	}
	
}
