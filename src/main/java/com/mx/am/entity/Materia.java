package com.mx.am.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@Table(name = "t_materias", catalog = "escuela")
public class Materia implements Serializable {
	
	private static final long serialVersionUID = -5183145573400497260L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_t_materias")
	private Long idTMaterias;
	
	@Column(name = "nombre", length = 80)
	private String nombre;
	
	@Column(name = "activo")
	private boolean activo;
}
