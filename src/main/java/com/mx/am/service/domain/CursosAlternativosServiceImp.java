package com.mx.am.service.domain;

import com.mx.am.dto.CalificacionWrapper;
import com.mx.am.dto.examples.Response200;
import com.mx.am.entity.Alumnos;
import com.mx.am.entity.Calificacion;
import com.mx.am.entity.Materia;
import com.mx.am.repository.AlumnosRepository;
import com.mx.am.repository.CalificacionRepository;
import com.mx.am.repository.MateriasRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service("serviceImpl")
public class CursosAlternativosServiceImp /*implements CursosAlternativosService*/ {
	
	@Autowired
	private AlumnosRepository alumnosRepository;
	
	@Autowired
	private MateriasRepository materiasRepository;
	
	@Autowired
	private CalificacionRepository calificacionRepository;
	
	public Optional<Calificacion> buscarCalificacionById(Long idCalificacion) {
		return calificacionRepository.findById(idCalificacion);
	}
	
    public CalificacionWrapper findCalificacionByAlumnosId(Long idTUsuario) {
    	List<Calificacion> respuestaDao = calificacionRepository.findCalificacionByAlumnosId(idTUsuario);
    	if(respuestaDao.isEmpty()) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Alumnno con el Id: %d no existe", idTUsuario));
    	}else {
    		double promedio = calificacionRepository.obtenerCalificacionPromedioByAlumno(idTUsuario);
    		return new CalificacionWrapper(respuestaDao, promedio);
    	}
    }
	
    public List<Calificacion> findCalificacionByMateriaIdAndAlumnoId(Long idTMateria, Long idTUsuario) {
    	List<Calificacion> respuestaDao = calificacionRepository.findCalificacionByMateriaIdAndAlumnoId(idTMateria, idTUsuario);
    	return respuestaDao;
    }
    
	public List<Alumnos> getAlumnos() {
		List<Alumnos> respuestaDao = alumnosRepository.findAll();
    	if(respuestaDao.isEmpty()) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sin registos encontrados en alumnos");
    	}else {
    		return respuestaDao;
    	}
	}

	public List<Materia> getMaterias() {
		List<Materia> respuestaDao = materiasRepository.findAll();
    	if(respuestaDao.isEmpty()) {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sin registos encontrados en materias");
    	}else {
    		return respuestaDao;
    	}
	}
	
	public Response200 guardarCalificacion(Calificacion calificacion) {
		List<Calificacion> respuestaDao = findCalificacionByMateriaIdAndAlumnoId(calificacion.getMateria().getIdTMaterias(), calificacion.getAlumnos().getIdUsuario());
    	if(respuestaDao.isEmpty()) {
    		calificacion.setFechaRegistro(new Date());
    		calificacionRepository.save(calificacion);
    		return new Response200("ok", "calificacion registrada");
    	}else {
    		String alumno = calificacion.getAlumnos().getNombre().concat(" ").concat(calificacion.getAlumnos().getApPaterno()).concat(calificacion.getAlumnos().getApMaterno());
    		return new Response200("error", "No es posible guardar materias duplicadas para el alumno " + alumno +", favor de validar");
    		//throw new ResponseStatusException(HttpStatus.CONFLICT, "No es posible guardar materias duplicadas para el alumno " + alumno +", favor de validar");
    	}
	}
	
	public Response200 actualizaCalificacion(Long idAlumno, Calificacion calificacion) {
		List<Calificacion> respuestaDao = findCalificacionByMateriaIdAndAlumnoId(calificacion.getMateria().getIdTMaterias(), idAlumno);
    	if(respuestaDao.isEmpty()) {
    		String alumno = calificacion.getAlumnos().getNombre().concat(" ").concat(calificacion.getAlumnos().getApPaterno()).concat(calificacion.getAlumnos().getApMaterno());
    		return new Response200("error", "No es posible actualizar ya que no existe un registro de materia(s) para " + alumno +", favor de validar");
    		//throw new ResponseStatusException(HttpStatus.CONFLICT, "No es posible actualizar ya que no existe un registro de materia(s) para " + alumno +", favor de validar");
    	}else {
    		respuestaDao.get(0).setCalificacion(calificacion.getCalificacion());
    		respuestaDao.get(0).setFechaRegistro(new Date());
    		calificacionRepository.save(respuestaDao.get(0));
    		return new Response200("ok", "calificacion actualizada"); 
    	}
	}
	
	public Response200 eliminarCalificacion(Long idCalificacion) {
		Optional<Calificacion> objCalif = buscarCalificacionById(idCalificacion);
		if(objCalif.isPresent()) {
			calificacionRepository.delete(objCalif.get());
			return new Response200("ok", "calificacion eliminada");
		}else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No se encontraron registros con el idCalificacion: %d", idCalificacion));
		}
	}
	
}
