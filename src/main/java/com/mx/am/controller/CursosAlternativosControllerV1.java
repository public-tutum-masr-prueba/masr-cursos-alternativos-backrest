package com.mx.am.controller;

import com.mx.am.dto.CalificacionWrapper;
import com.mx.am.dto.examples.*;
import com.mx.am.entity.Alumnos;
import com.mx.am.entity.Calificacion;
import com.mx.am.entity.Materia;
import com.mx.am.service.domain.CursosAlternativosServiceImp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/cursos-alternativos/")
@Api(value = "CursosAternativos", tags = "Cursos-Aternativos")
public class CursosAlternativosControllerV1 {
	
	@Autowired
	@Qualifier("serviceImpl")
	private CursosAlternativosServiceImp service;
    
	@GetMapping(value = "/calificaciones/alumno/{idAlumno}", produces = {"application/json"})
    @ApiOperation(
            nickname = "Microservice Info Region and subregion",
            value = "Region and Subregion",
            notes = "Displays microservice info.",
            produces = "application/json",
            response = Calificacion.class
    )
    @ApiResponses(value = {
		@ApiResponse(code = 200, message = "ok", response = Response200.class),
        @ApiResponse(code = 400, message = "Bad request.", response = ResponseBase.Response400.class),
        @ApiResponse(code = 404, message = "Resource not found.", response = ResponseBase.Response404.class),
        @ApiResponse(code = 500, message = "Internal Server Error.", response = ResponseBase.Response500.class),
        @ApiResponse(code = 503, message = "Service Unavailable.", response = ResponseBase.Response503.class)
    })
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "version", value = "v1", required = true, dataTypeClass = String.class, paramType = "header", type = "String"),
	})
    //@RequestParam(name = "arrayIatas", required = true)
    public ResponseEntity<Object> getCalificacionByAlumno(@PathVariable(name = "idAlumno", required = true) final Long idAlumno, HttpServletRequest requestHttp) {
		printHeadersRequest(requestHttp);
		CalificacionWrapper response = service.findCalificacionByAlumnosId(idAlumno);
	    return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "alumnos", produces = {"application/json"})
    public ResponseEntity<Object> getAlumnos(/*HttpServletRequest requestHttp*/) {
		List<Alumnos> responseAlumnos = service.getAlumnos();
	    return new ResponseEntity<>(responseAlumnos, HttpStatus.OK);
    }
    
    @GetMapping(value = "/materias", produces = {"application/json"})
    public ResponseEntity<Object> getMaterias(HttpServletRequest requestHttp) {
		List<Materia> responseAlumnos = service.getMaterias();
	    return new ResponseEntity<>(responseAlumnos, HttpStatus.OK);
    }
    
    @PostMapping(value = "/calificaciones/guardar", produces = {"application/json"})
    public ResponseEntity<Object> guardarCalificacion(@RequestBody Calificacion calificacion, HttpServletRequest requestHttp) throws ResponseStatusException {
	    return new ResponseEntity<>(service.guardarCalificacion(calificacion), HttpStatus.CREATED);
    }
    
    @PutMapping(value = "/calificaciones/actualizar/alumno/{idAlumno}", produces = {"application/json"})
    public ResponseEntity<Object> actualizaCalificacion(@PathVariable(name = "idAlumno", required = true) final Long idAlumno, @RequestBody Calificacion calificacion) {
	    return new ResponseEntity<>(service.actualizaCalificacion(idAlumno, calificacion), HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/calificaciones/eliminar/{idCalificacion}", produces = {"application/json"})
    public ResponseEntity<Object> eliminarCalificacion(@PathVariable(name = "idCalificacion", required = true) final Long idCalificacion, HttpServletRequest requestHttp) {
	    return new ResponseEntity<>(service.eliminarCalificacion(idCalificacion), HttpStatus.OK);
    }
    
    private void printHeadersRequest(HttpServletRequest requestHttp) {
        HttpHeaders httpHeaders = new HttpHeaders();
        for (String h : Collections.list(requestHttp.getHeaderNames())) {
            httpHeaders.put(h, Collections.list(requestHttp.getHeaders(h)));
        }
        System.out.println(httpHeaders);
        System.out.println(requestHttp.getRequestURI());
    }
}
