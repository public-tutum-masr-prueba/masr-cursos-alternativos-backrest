package com.mx.am.repository;

import com.mx.am.entity.Calificacion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CalificacionRepository extends JpaRepository<Calificacion, Long> {
	
	//@Query(value = "select c from Calificacion c where c.alumnos.idUsuario = :idTUsuario")
	@Query(value = "select id_t_calificaciones, id_t_materias, id_t_usuarios, calificacion, fecha_registro from t_calificaciones where id_t_usuarios = :idTUsuario", nativeQuery = true)
    public List<Calificacion> findCalificacionByAlumnosId(@Param("idTUsuario") Long idTUsuario);
	
	@Query(value = "select id_t_calificaciones, id_t_materias, id_t_usuarios, calificacion, fecha_registro "
			+ "from t_calificaciones where id_t_materias = :idTMateria and id_t_usuarios = :idTUsuario", nativeQuery = true)
	public List<Calificacion> findCalificacionByMateriaIdAndAlumnoId(@Param("idTMateria") Long idTMateria, @Param("idTUsuario") Long idTUsuario);
	
	@Query(value = "SELECT avg(calificacion) as promedio from t_calificaciones where id_t_usuarios = :idTUsuario", nativeQuery = true)
	public Double obtenerCalificacionPromedioByAlumno(@Param("idTUsuario") Long idTUsuario);
}
