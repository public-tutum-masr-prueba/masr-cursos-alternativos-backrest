package com.mx.am.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.am.entity.Alumnos;

@Repository
public interface AlumnosRepository extends JpaRepository<Alumnos, Long> {
	
}
