package com.mx.am.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.am.entity.Calificacion;
import com.mx.am.entity.Materia;

@Repository
public interface MateriasRepository extends JpaRepository<Materia, Long> {

}
