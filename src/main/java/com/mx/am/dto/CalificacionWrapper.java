package com.mx.am.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

import com.mx.am.entity.Calificacion;
@Getter
@Setter
public class CalificacionWrapper implements Serializable {
    private static final long serialVersionUID = -4079426912566880635L;
    private List<Calificacion> listaCalificacion;
    private double promedio;
    
    public CalificacionWrapper() {
    	
    }
    
    public CalificacionWrapper(List<Calificacion> listaCalificacion, double promedio) {
    	this.listaCalificacion = listaCalificacion;
    	this.promedio = promedio;
    }
}
