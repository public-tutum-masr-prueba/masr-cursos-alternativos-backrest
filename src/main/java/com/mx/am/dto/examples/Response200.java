package com.mx.am.dto.examples;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@ApiModel
@Setter
@Getter
public class Response200 {
	
      @ApiModelProperty(example = "ok", name = "success", dataType = "string", position = 1)
      private String success;

      @ApiModelProperty(example = "calificacion registrada", name = "msg", dataType = "string", position = 2)
      private String msg;
      
      public Response200() {
    	  
      }
      
      public Response200(String success, String msg) {
    	  this.success = success;
    	  this.msg = msg;
      }
}
