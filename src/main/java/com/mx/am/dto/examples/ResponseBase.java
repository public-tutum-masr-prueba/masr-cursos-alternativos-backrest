package com.mx.am.dto.examples;

import java.util.List;

import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel
@Setter
@Getter
public class ResponseBase {

    @ApiModelProperty(example = "5826871a-3ae5-4755-b173-459fa28c58de", name = "transactionID", dataType = "string", position = 2)
    private String transactionID;

    @ApiModelProperty(example = "2021-04-07 16:21:36:03", name = "timestamp", dataType = "string", position = 4)
    private String timestamp;

    @ApiModelProperty(example = "ABORT", name = "event", dataType = "string", position = 7)
    private String event;

    @ApiModelProperty(example = "details", name = "details", dataType = "string", position = 8)
    private String details;

    @ApiModel
    @Setter
    @Getter
    public static class Response400 extends ResponseBase {
    	@ApiModelProperty(example = "BAD_REQUEST", name = "httpStatus", dataType = "enum", position = 3)
    	private HttpStatus httpStatus;
    	
    	@ApiModelProperty(example = "INVALID_HEADERS", name = "errorCode", dataType = "string", position = 5)
    	private String errorCode;
    	
    	@ApiModelProperty(example = "Invalid headers, check your information", name = "message", dataType = "string", position = 6)
    	private String message;
    }
    
    @ApiModel
    @Setter
    @Getter
    public static class Response401 extends ResponseBase {
    	@ApiModelProperty(example = "UNAUTHORIZED", name = "httpStatus", dataType = "enum", position = 3)
    	private HttpStatus httpStatus;
    	  
    	@ApiModelProperty(example = "UNAUTHORIZED", name = "errorCode", dataType = "string", position = 5)
    	private String errorCode;
    	
    	@ApiModelProperty(example = "Unauthorized", name = "message", dataType = "string", position = 6)
    	private String message;
    }
    
    @ApiModel
    @Setter
    @Getter
    public static class Response403 extends ResponseBase {   	
    	@ApiModelProperty(example = "FORBIDDEN", name = "httpStatus", dataType = "enum", position = 3)
    	private HttpStatus httpStatus;
    	
    	@ApiModelProperty(example = "FORBIDDEN", name = "errorCode", dataType = "string", position = 5)
    	private String errorCode;
    	
    	@ApiModelProperty(example = "Forbidden", name = "message", dataType = "string", position = 6)
    	private String message;
    }
    
    @ApiModel
    @Setter
    @Getter
    public static class Response404 extends ResponseBase {
    	@ApiModelProperty(example = "RESOURCE_NOT_FOUND", name = "httpStatus", dataType = "enum", position = 3)
    	private HttpStatus httpStatus;
    	  
    	@ApiModelProperty(example = "RESOURCE_NOT_FOUND", name = "errorCode", dataType = "string", position = 5)
    	private String errorCode;
    	
    	@ApiModelProperty(example = "Not Found", name = "message", dataType = "string", position = 6)
    	private String message;
    }
    
    @ApiModel
    @Setter
    @Getter
    public static class Response503 extends ResponseBase {
	    @ApiModelProperty(example = "SERVICE_UNAVAILABLE", name = "httpStatus", dataType = "enum", position = 3)
	    private HttpStatus httpStatus;

	    @ApiModelProperty(example = "SERVICE_UNAVAILABLE", name = "errorCode", dataType = "string", position = 5)
	    private String errorCode;

	    @ApiModelProperty(example = "Service Unavailable", name = "message", dataType = "string", position = 6)
	    private String message;
    }
    
    @ApiModel
    @Setter
    @Getter
    public static class Response500 extends ResponseBase {
        @ApiModelProperty(example = "INTERNAL_SERVER_ERROR", name = "httpStatus", dataType = "enum", position = 3)
        private HttpStatus httpStatus;
        
        @ApiModelProperty(example = "INTERNAL_SERVER_ERROR", name = "errorCode", dataType = "string", position = 5)
        private String errorCode;

        @ApiModelProperty(example = "Internal Server Error", name = "message", dataType = "string", position = 6)
        private String message;
    }
}
