package com.mx.am.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
        		//.select().apis(RequestHandlerSelectors.any())
                .select().apis(RequestHandlerSelectors.basePackage("com.mx.am.controller"))
                .paths(PathSelectors.any())
                //.paths(PathSelectors.regex("/api/.*"))
                .build()
                .useDefaultResponseMessages(false);
    }
    
    @Bean
    ApiInfo apiInfo() {
        Contact contact = new Contact(
                "Phoenix Team",
                "https://dev-portal.com/reassociation",
                "phoenix-team@aeromexico.com");
        return new ApiInfoBuilder()
                .title("Escuela Cursos Alternativos")
                .description("Rest API")
                .version("1.0.0-SNAPSHOT")
                .license("Apache 2.0")
                .contact(contact)
                .build();
    }
}
