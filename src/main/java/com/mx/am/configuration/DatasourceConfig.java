package com.mx.am.configuration;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatasourceConfig {
	
    @Value("${spring.datasource.url}")
    private String url;
    
    @Value("${spring.datasource.username}")
    private String username;
    
    @Value("${spring.datasource.password}")
    private String password;
    
    @Value("${spring.datasource.loginTimeout}")
    private int loginTimeout;
    
    private static final String POSTGRESQL_DRIVER = "org.postgresql.Driver";

    @Bean
    public DataSource getDataSource() throws SQLException {
          DataSource build = DataSourceBuilder.create()
                  .driverClassName(POSTGRESQL_DRIVER)
                  .url(url)
                  .username(username)
                  .password(password)
                  .build();
          build.setLoginTimeout(loginTimeout);
          return build;
    }
}
